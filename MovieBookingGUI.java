package moviesession;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author MaitryMehta Student Id - 17964028 MovieBookingGUI extends JPanel and
 * implements interface ActionListener and ListSelectionListener All the
 * JComponents which will be used in designing a gui for movie booking system is
 * initialized.
 */
public class MovieBookingGUI extends JPanel implements ActionListener, ListSelectionListener {

    private JButton[][] seatsButton;
    private JLabel theaterLabel;
    private JRadioButton adultRadio, elderlyRadio, childRadio;
    private JList movieList;
    private DefaultListModel model;
    private JCheckBox complementaryCheck;
    private JButton exitButton, cancelButton, bookButton;
    private ListSelectionListener listener;
    private ArrayList<SeatReservation> currentReservation;

    /**
     *
     * Constructor takes in list of movieSessions as a parameter. Super method
     * is called to use the layout manager for the GUi
     *
     */
    public MovieBookingGUI(List<MovieSession> movieSessions) {
        super(new BorderLayout());
        initalizeMovieSeatings();
        initalizeTheaterTitle();
        initalizeMovieSessions(movieSessions);
        initalizeBookingOptions();
        currentReservation = new ArrayList<>();
    }

    /**
     * The method initializeMovieSeatings sets the theater seats in the center
     * panel of border layout. This seats are set on a new panel with gridLayout
     */
    private void initalizeMovieSeatings() {
        JPanel movieSeatingsPanel = new JPanel(new GridLayout(8, 6));
        movieSeatingsPanel.setPreferredSize(new Dimension(500, 500));
        ButtonListener listener = new ButtonListener();
        seatsButton = new JButton[MovieSession.NUM_ROWS][MovieSession.NUM_COLS];
        for (int i = 0; i < MovieSession.NUM_ROWS; i++) {
            for (int j = 0; j < MovieSession.NUM_COLS; j++) {
                seatsButton[i][j] = new JButton(MovieSession.convertIndextoRow(i) + "," + j);
                seatsButton[i][j].addActionListener(listener);
                movieSeatingsPanel.add(seatsButton[i][j]);

            }
        }
        add(movieSeatingsPanel, BorderLayout.CENTER);
    }

    /**
     * The method initializeMovieTheaterTitle is called to set name of the
     * theater in the north panel of border layout.
     */
    private void initalizeTheaterTitle() {
        theaterLabel = new JLabel("Movies N Movies");
        JPanel titlePanel = new JPanel();
        titlePanel.add(theaterLabel);
        add(theaterLabel, BorderLayout.NORTH);
    }

    /**
     *
     * The method initalizeMovieSessions is called to set the list of movies in
     * the east panel of border layout.
     */
    private void initalizeMovieSessions(List<MovieSession> movieSessions) {
        model = new DefaultListModel();
        for (MovieSession session : movieSessions) {
            model.addElement(session);
        }
        movieList = new JList(movieSessions.toArray());
        movieList.setVisibleRowCount(50);
        movieList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        movieList.addListSelectionListener(listener);
        add(new JScrollPane(movieList), BorderLayout.EAST);
    }

    /**
     * The method initalizeBookingOptions() is called to set the buttons for the
     * different functionality in the south panel of border layout.
     */
    private void initalizeBookingOptions() {
        JPanel southPanel = new JPanel();
        adultRadio = new JRadioButton("Adult");
        elderlyRadio = new JRadioButton("Elderly");
        childRadio = new JRadioButton("Child");

        exitButton = new JButton("Exit");
        cancelButton = new JButton("Cancel");
        bookButton = new JButton("Book");
        complementaryCheck = new JCheckBox("Complementary");

        exitButton.addActionListener(this);
        cancelButton.addActionListener(this);
        bookButton.addActionListener(this);
        complementaryCheck.addActionListener(this);

        southPanel.add(adultRadio);
        southPanel.add(elderlyRadio);
        southPanel.add(childRadio);
        southPanel.add(complementaryCheck);
        southPanel.add(exitButton);
        southPanel.add(cancelButton);
        southPanel.add(bookButton);
        add(southPanel, BorderLayout.SOUTH);
    }

    /**
     *
     * This action performed handles the even for exit , cancel and book buttons
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == exitButton) {
            System.exit(0);
        }
        if (e.getSource() == cancelButton) {
            currentReservation.clear();
        }
        if (e.getSource() == bookButton) {
            boolean flag = false;
            if (flag) {
                JOptionPane.showMessageDialog(this, "Perhaps seat has been Filled or trying to book thchild in an adult movie", "Booking Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, " Ticket cost" + +JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    /**
     * The Button listener class handles events for the selecting the movie,
     * seats It also looks which radio buttons are pressed and performs
     * functions accordingly
     */
    public class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            StringTokenizer tokenizer = new StringTokenizer(e.getActionCommand(), ",");
            if (!movieList.isSelectionEmpty()) {

                char rowChar = tokenizer.nextToken().charAt(0);
                int row = MovieSession.convertRowToIndex(rowChar);
                int col = Integer.parseInt(tokenizer.nextToken());

                MovieSession currentSession = (MovieSession) model.getElementAt(movieList.getSelectedIndex());

                if (currentSession.isSeatAvailable(rowChar, col)) {
                    SeatReservation newReserve;
                    seatsButton[row][col].setEnabled(false);
                    if (childRadio.isSelected()) {
                        newReserve = new ChildReservation(rowChar, col);
                        seatsButton[row][col].setForeground(Color.BLUE);
                        currentReservation.add(newReserve);
                    }
                    if (adultRadio.isSelected()) {
                        seatsButton[row][col].setForeground(Color.WHITE);
                        newReserve = new AdultReservation(rowChar, col);
                        currentReservation.add(newReserve);
                    }
                    if (elderlyRadio.isSelected()) {
                        seatsButton[row][col].setForeground(Color.BLUE);
                        newReserve = new ElderlyReservation(rowChar, col);
                        currentReservation.add(newReserve);
                    }
                }
            }
        }

    }

    public static void main(String[] args) {
        List<MovieSession> movieSessions = new ArrayList<>();

        MovieSession cinema1 = new MovieSession("Logan", 'R', new Time(10, 10, 10));
        MovieSession cinema2 = new MovieSession("Baby Boss", 'G', new Time(11, 00, 00));
        MovieSession cinema3 = new MovieSession("Guardians Of The Galaxy ", 'M', new Time(9, 30, 00));
        MovieSession cinema4 = new MovieSession("Moana", 'G', new Time(13, 45, 00));

        movieSessions.add(cinema1);
        movieSessions.add(cinema2);
        movieSessions.add(cinema3);
        movieSessions.add(cinema4);

        Collections.sort(movieSessions);// sorts the list of movies

        MovieBookingGUI myPanel = new MovieBookingGUI(movieSessions);//setting panel for the moviebooking Gui
        JFrame frame = new JFrame("Movie N Movies"); //create frame to hold our JPanel subclass	
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(myPanel);  //add instance of MyGUI to the frame
        frame.pack(); //resize frame to fit our Jpanel

        //Position frame on center of screen 
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int screenHeight = d.height;
        int screenWidth = d.width;
        frame.setLocation(new Point((screenWidth / 2) - (frame.getWidth() / 2),
                (screenHeight / 2) - (frame.getHeight() / 2)));

        //show the frame	
        frame.setVisible(true);
    }
}
