package moviesession;

/**
 * @author MaitryMehta Student Id - 17964028
 */
public abstract class SeatReservation {

    private final float COMPLEMENTARY = 0;

    private char row;
    private int col;
    protected boolean complementary;

    /*
    *A  SeatReservation constructor with row & col passed into it as parameter 
     */
    public SeatReservation(char row, int col) {
        this.row = row;
        this.col = col;
    }

    /*
    *An absract Getter method returns ticket price from adult reservation, child reservation and edult reservation respectively
     */
    public abstract float getTicketPrice();

    /*
    *Getter method returns rows where the seat is booked
     */
    public char getRow() {
        return row;
    }

    /*
    *Getter method returns columns where the seat is booked
     */
    public int getCol() {
        return col;
    }

    /*
    * A setter method to check whether the seat is complementary 
     */
    public void setComplementary(boolean complementary) {
        this.complementary = complementary;
    }

}
