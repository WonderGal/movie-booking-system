package moviesession;

/**
 * @author MaitryMehta Student Id - 17964028
 */
public class ChildReservation extends SeatReservation {

    public final float COMPLEMENTARY = 0;
    public final float CHILD_TICKET_PRICE = 8;

    /*
    *Child Reservation  constructor extends super class SeatReservation and two parameters row and column are passesd into it
     */
    public ChildReservation(char row, int col) {
        super(row, col);
    }

    /*
    *Override Getter method return the ticket price for child reservation
     */
    @Override
    public float getTicketPrice() {
        if (this.complementary = true) {
            return COMPLEMENTARY;
        } else {
            return CHILD_TICKET_PRICE;
        }
    }

}
