package moviesession;

/**
 * @author MaitryMehta Student Id - 17964028
 */
public class AdultReservation extends SeatReservation {

    private final float COMPLEMENTARY = 0;
    public final float ADULT_TICKET_PRICE = 12.50f;

    /*
    *Adult Reservation  constructor extends super class SeatReservation and two parameters row and column are passesd into it
     */
    public AdultReservation(char row, int col) {
        super(row, col);
    }

    /*
    *Override Getter method return the ticket price for adult reservation
     */
    @Override
    public float getTicketPrice() {
        if (this.complementary = true) {
            return COMPLEMENTARY;
        } else {
            return ADULT_TICKET_PRICE;
        }
    }

}
