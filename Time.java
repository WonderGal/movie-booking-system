package moviesession;

/**
 * @author MaitryMehta Student Id - 17964028
 */
public class Time implements Comparable<Time> {

    private int hours;
    private int mins;
    private int secs;

    /*
    * A default Time constructor with no parameter passed into it
     */
    public Time() {
        this.secs = 0;
        this.mins = 0;
        this.hours = 0;
    }

    /*
    *A  Time constructor with hours  passed into it as parameter 
     */
    public Time(int hours) {
        this.hours = hours;
    }

    /*A  Time constructor with hours & minutes passed into it as parameter  
    *
     */
    public Time(int hours, int mins) {
        this.hours = hours;
        this.mins = mins;
    }

    /*
     *A  Time constructor with hours , minutes & seconds passed into it as parameter 
     */
    public Time(int hours, int mins, int secs) {
        this.hours = hours;
        this.mins = mins;
        this.secs = secs;
    }

    /*
*Getter method returns seconds
     */
    public int getSeconds() {
        return this.secs;
    }

    /*
    *Getter method returns minutes
     */
    public int getMinutes() {
        return this.mins;
    }

    /*
    *Getter method returns hours
     */
    public int getHours() {
        return this.hours;
    }

    /*
    *Setter method sets seconds
     */
    public void setSeconds(int secs) {
        if (secs >= 0 && secs <= 59) {
            this.secs = secs;
        } else {
            System.out.println("INVALID SECONDS");
        }
    }

    /*
    *Setter method sets minutes
     */
    public void setMinutes(int mins) {
        if (mins >= 0 && mins <= 59) {
            this.mins = mins;
        } else {
            System.out.println("INVALID MINUTES");
        }
    }

    /*
    *Setter method sets hours 
     */
    public void setHours(int hours) {
        if (hours >= 0 && hours <= 23) {
            this.hours = hours;
        } else {
            System.out.println("INVALID HOURS");
        }

    }

    /*
    *ToString method to print time in a readable format
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", hours, mins, secs);
    }

    /*
    *Boolean to check if two different times are considered equal if both have hours, minutes and seconds.
     */
    public boolean equals(Time otherTime) {
        return this.hours == otherTime.hours && this.mins == otherTime.mins && this.secs == otherTime.secs;
    }

    /*
    *Override a CompareTo method of Comparable interface comparing other time so that the earlier time takes precedence
     */
    @Override
    public int compareTo(Time currentTime) {
        {

            if (this.hours < currentTime.hours) {
                return -1;
            } else if (this.hours > currentTime.hours) {
                return 1;
            }
            if (this.mins < currentTime.mins) {
                return -1;
            } else if (this.mins > currentTime.mins) {
                return 1;
            }
            if (this.secs < currentTime.secs) {
                return -1;
            } else if (this.secs > currentTime.secs) {
                return 1;
            } else {
                return 0;
            }
        }
    }

}
