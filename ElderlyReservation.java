package moviesession;

/*
 *Elderly reservation extends adult reservation so that the ticket price of adult can be used in elderly reservation
 */
public class ElderlyReservation extends AdultReservation {

    private final float COMPLEMENTARY = 0;
    private final float ELDERLY_TICKET_PRICE = (this.ADULT_TICKET_PRICE * 70) / 100;

    /*
    *Elderly Reservation  constructor extends super class SeatReservation and two parameters row and column are passesd into it
     */
    public ElderlyReservation(char row, int col) {
        super(row, col);
    }

    /*
    *Override Getter method return the ticket price for elderly reservation
     */
    @Override
    public float getTicketPrice() {
        if (this.complementary = true) {
            return COMPLEMENTARY;
        } else {
            return ELDERLY_TICKET_PRICE;
        }
    }

}
