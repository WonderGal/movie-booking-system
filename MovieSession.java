package moviesession;

/**
 * @author MaitryMehta Student Id - 17964028
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
*MovieSession class implements the methods of comparable interface
 */
public class MovieSession implements Comparable<MovieSession> {

    private String movieName;
    private char rating;
    private Time sessionTime;
    private SeatReservation[][] sessionSeats;
    public static int NUM_ROWS = 8;
    public static int NUM_COLS = 6;

    /*
    * A constructor that pass movieName, rating and sessionTime
    * It also dimensional array of SeatReservation which references sessionSeats
     */
    public MovieSession(String movieName, char rating, Time sessionTime) {
        this.movieName = movieName;
        this.rating = rating;
        this.sessionTime = sessionTime;
        sessionSeats = new SeatReservation[NUM_ROWS][NUM_COLS];
    }

    /**
     * Static methods used to convert Array charRow to Index
     */
    public static int convertRowToIndex(char rowLetter) {
        int index = (int) (rowLetter - 65);
        return index;
    }

    /**
     * Static methods used to convert Array Index to row
     */
    public static char convertIndextoRow(int rowIndex) {
        char row = (char) (rowIndex + 65);
        return row;
    }

    /**
     * Getter method returns rating of movie
     */
    public char getRating() {
        return rating;
    }

    /**
     * Getter method returns movie name
     */
    public String getMovieName() {
        return movieName;
    }

    /**
     * Getter method returns movie name
     */
    public Time getSessionTime() {
        return sessionTime;
    }

    /**
     *
     * This getter method returns the Seat reservation held at a specific row
     * and column
     */
    public SeatReservation getSeat(char row, int col) {
        int rowIndex = convertRowToIndex(row);
        return sessionSeats[rowIndex][col];
    }

    /**
     *
     * This setter method sets the Seat reservation held at a specific row and
     * column
     */
    public void setSeat(char row, int col, SeatReservation resvType) {
        int rowIndex = convertRowToIndex(row);
        sessionSeats[rowIndex][col] = resvType;
    }

    /**
     *
     * This method returns true if a seat at specific row and column has not
     * been previously booked
     */
    public boolean isSeatAvailable(char row, int col) {
        int rowIndex = convertRowToIndex(row);
        return sessionSeats[rowIndex][col] == null;
    }

    /**
     * This method looks through the each reservation and ensures that the
     * designated row and col is available. If the sessionSeats are available
     * that the boolean condition is set to true or it will be false. A counter
     * for adult reservation is set, in which elderly is also considered as
     * adult reservation. This helps in finding that the child Reservation is
     * not made in an R rated movie and in M rated movie need adult to accompany
     * Related error messages will be displayed to direct the customers. If all
     * the conditions are meet the booking will be made successfully
     */
    public boolean applyBookings(List<SeatReservation> reservations) {
        boolean flag = true;
        int adult = 0;
        for (SeatReservation eachReservations : reservations) {
            if (eachReservations instanceof AdultReservation || eachReservations instanceof ElderlyReservation) {
                adult++;
            }
            if (!isSeatAvailable(eachReservations.getRow(), eachReservations.getCol())) {
                System.out.println("Sorry seats are already taken");
                flag = false;
            }
            if (eachReservations instanceof ChildReservation && this.getRating() == 'R') {
                System.out.println("Cannot book child in R rated movie");
                flag = false;
            }
            if (this.getRating() == 'M' && adult > 0) {
                flag = true;
            } else if (this.getRating() == 'M' && adult == 0) {
                System.out.println("Cannot book unsuperviesed child in M rated movie");
            }
        }
        if (flag) {
            for (SeatReservation eachReservations : reservations) {
                if (isSeatAvailable(eachReservations.getRow(), eachReservations.getCol())) {
                    setSeat(eachReservations.getRow(), eachReservations.getCol(), eachReservations);
                }
            }
        }
        return flag;
    }

    /**
     * A print of seats will be made, were E represents Elderly reservation, A
     * represents Adult reservation and C represents child reservation
     */
    public void printSeats() {
        for (int i = 0; i < sessionSeats.length; i++) {
            for (int j = 0; j < sessionSeats[i].length; j++) {
                if (sessionSeats[i][j] == null) {
                    System.out.print("|_|");
                }
                if (sessionSeats[i][j] instanceof ChildReservation) {
                    System.out.print("|C|");
                }
                if (sessionSeats[i][j] instanceof ElderlyReservation) {
                    System.out.print("|E|");
                } else if (sessionSeats[i][j] instanceof AdultReservation) {
                    System.out.print("|A|");
                }
            }
            System.out.println();
        }
    }

    /**
     *
     * This to string method return string of movie name with rating and timing
     */
    @Override
    public String toString() {
        return this.movieName + " (" + this.rating + ") " + this.sessionTime;
    }

    /**
     * Override the comparable interface to compare two movieSessions where the
     * earlier session time takes precedence
     */
    @Override
    public int compareTo(MovieSession currentMovieSession) {
        if (getSessionTime().compareTo(currentMovieSession.getSessionTime()) > 0) {
            return 1;
        } else if (getSessionTime().compareTo(currentMovieSession.getSessionTime()) < 0) {
            return -1;
        } else if (getSessionTime().compareTo(currentMovieSession.getSessionTime()) == 0) {
            if (getMovieName().compareTo(currentMovieSession.getMovieName()) > 0) {
                return 1;
            } else if (getMovieName().compareTo(currentMovieSession.getMovieName()) < 0) {
                return -1;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        /*
       * An array list of different movie Sessions with different times and rating are created
         */
        List<MovieSession> movieSessions = new ArrayList<MovieSession>();

        MovieSession cinema1 = new MovieSession("Logan", 'R', new Time(10, 10, 10));
        MovieSession cinema2 = new MovieSession("Baby Boss", 'G', new Time(11, 00, 00));
        MovieSession cinema3 = new MovieSession("Guardians Of The Galaxy ", 'M', new Time(9, 30, 00));
        MovieSession cinema4 = new MovieSession("Moana", 'G', new Time(13, 45, 00));

        movieSessions.add(cinema1);
        movieSessions.add(cinema2);
        movieSessions.add(cinema3);
        movieSessions.add(cinema4);

        Collections.sort(movieSessions); // java.util.collections class is used to sort the movies in order.
        System.out.println(movieSessions);

        //Testing different bookings situations
        SeatReservation child = new ChildReservation('G', 1);
        SeatReservation child1 = new ChildReservation('C', 2);

        SeatReservation elderly = new ElderlyReservation('A', 3);
        SeatReservation elderly1 = new ElderlyReservation('D', 4);

        SeatReservation adult = new AdultReservation('A', 0);
        SeatReservation adult1 = new AdultReservation('E', 5);

        //TESTING TO BOOK TICKETS IN CINEMA 1 
        ArrayList<SeatReservation> movieReservation1 = new ArrayList<>();
        System.out.println("TRYING TO BOOK ONLY CHILD TICKET FOR THE R MOVIE");
        movieReservation1.add(child);
        cinema1.applyBookings(movieReservation1);
        cinema1.printSeats();
        System.out.println("----------------------------------");

        //TESTING TO BOOK TICKETS IN CINEMA 2
        ArrayList<SeatReservation> movieReservation2 = new ArrayList<>();
        System.out.println("TRYING TO BOOK ADULT AND ELDERLY TICKET FOR THE MOVIE");
        movieReservation2.add(elderly);
        movieReservation2.add(adult1);
        movieReservation2.add(elderly);
        cinema2.applyBookings(movieReservation2);
        cinema2.printSeats();
        System.out.println("----------------------------------");

        //TESTING TO BOOK TICKETS IN CINEMA 3
        ArrayList<SeatReservation> movieReservation3 = new ArrayList<>();
        System.out.println("TRYING TO BOOK CHILD,ELDERLY TICKET FOR  M  MOVIE");
        movieReservation3.add(elderly);
        movieReservation3.add(child1);
        cinema3.applyBookings(movieReservation3);
        cinema3.printSeats();
        System.out.println("----------------------------------");

        //TESTING TO BOOK TICKETS IN CINEMA 4
        ArrayList<SeatReservation> movieReservation4 = new ArrayList<>();
        System.out.println("TRYING TO BOOK CHILD TICKET FOR  G  MOVIE");
        movieReservation4.add(child1);
        cinema4.applyBookings(movieReservation4);
        cinema4.printSeats();
        System.out.println("----------------------------------");
    }
}
